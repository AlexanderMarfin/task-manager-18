package ru.tsc.marfin.tm.command.user;

import ru.tsc.marfin.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry user";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

}
