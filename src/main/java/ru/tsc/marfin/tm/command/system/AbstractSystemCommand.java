package ru.tsc.marfin.tm.command.system;

import ru.tsc.marfin.tm.api.service.ICommandService;
import ru.tsc.marfin.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
