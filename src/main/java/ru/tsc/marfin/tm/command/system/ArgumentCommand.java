package ru.tsc.marfin.tm.command.system;

import ru.tsc.marfin.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentCommand extends AbstractSystemCommand {

    public static final String NAME = "argument";

    public static final String DESCRIPTION = "Show application arguments";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command: commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
