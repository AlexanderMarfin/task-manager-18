package ru.tsc.marfin.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
