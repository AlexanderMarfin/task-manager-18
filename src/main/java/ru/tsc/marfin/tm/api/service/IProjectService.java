package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateStart, Date dateEnd);

    Project add(Project project);

    Project findOneByIndex(Integer index);

    Project findOneById(String id);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project remove(Project project);

    Project removeByIndex(Integer index);

    Project removeById(String id);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
