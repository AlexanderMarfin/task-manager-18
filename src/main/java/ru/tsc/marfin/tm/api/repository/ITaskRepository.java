package ru.tsc.marfin.tm.api.repository;

import ru.tsc.marfin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task add(Task task);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    void clear();

}
